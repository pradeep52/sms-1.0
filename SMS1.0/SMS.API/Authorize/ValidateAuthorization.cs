﻿using Microsoft.Owin.Security.OAuth;
using SMS.BAL.Repository;
using System.Threading.Tasks;

namespace SMS.API.Authorize
{
    public class ValidateAuthorization
    {
        public async Task<bool> IsAuthorized(OAuthGrantResourceOwnerCredentialsContext context)
        {
            using (var loginRepository = new LoginRepository())
            {
                var user = await loginRepository.Login(context.UserName, context.Password);
                return user != null;
            }
        }
    }
}