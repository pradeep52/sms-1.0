﻿using System.Threading.Tasks;
using Microsoft.Owin.Security.OAuth;
using System.Security.Claims;

namespace SMS.API.Authorize
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            try
            {
                ValidateAuthorization va = new ValidateAuthorization();
                if (await va.IsAuthorized(context))
                {                    
                    context.Validated(identity);
                }
                else
                {
                    context.SetError("Invalid Credentials", "Provided Username or Password is incorrect");
                    return;
                }
            }
            catch (System.Exception)
            {
                context.SetError("Invalid Credentials", "Provided Username or Password is incorrect");
                return;
            }
        }
    }

}