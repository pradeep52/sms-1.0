﻿using SMS.API.Models;
using SMS.BAL.IRepository;
using System.Threading.Tasks;
using System.Web.Http;

namespace SMS.API.Controllers
{
    public class LoginController : ApiController
    {
        private readonly ILoginRepository _loginRepository;
        public LoginController(ILoginRepository loginRepository)
        {
            this._loginRepository = loginRepository;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IHttpActionResult> Login([FromBody] Credentials u)
        {
            return Ok(await this._loginRepository.Login(u.UserName, u.Password));
        }

        [HttpPost]
        public async Task<IHttpActionResult> ResetPassword([FromBody]string email)
        {
            return Ok(await this._loginRepository.ResetPassword(email));
        }

        [HttpPost]
        public async Task<IHttpActionResult> ChangePassword([FromBody] Passwords p)
        {
            return Ok(await this._loginRepository.ChangePassword(p.OldPassword, p.NewPassword));
        }
    }
}
