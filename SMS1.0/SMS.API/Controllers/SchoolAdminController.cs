﻿using SMS.API.Models;
using SMS.BAL.IRepository;
using SMS.DATA;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace SMS.API.Controllers
{
    [RoutePrefix("api/school")]
    public class SchoolAdminController : ApiController
    {
        private readonly ISchoolRepository _schoolRepository;
        public SchoolAdminController(ISchoolRepository schoolAdminRepository)
        {
            this._schoolRepository = schoolAdminRepository;
        }

        [Authorize]
        [HttpPost]
        public IHttpActionResult AddUser([FromBody] AddStudentViewModel addStudentViewModel)
        {
            return Ok(this._schoolRepository.AddSchoolAdmin(addStudentViewModel.SchoolAdmin, addStudentViewModel.LoggedInSuperAdminId));
        }
        
        [Authorize]
        [HttpGet]
        public IHttpActionResult GetUsers(Guid schoolId)
        {
            return Ok(this._schoolRepository.GetUsers(schoolId));
        }

        [Authorize]
        [HttpPost]
        public IHttpActionResult AddSection([FromBody] Section section)
        {
            return Ok(this._schoolRepository.AddSection(section));
        }

        [Authorize]
        [HttpGet]
        public IHttpActionResult GetSectionsBySchool(Guid schoolId)
        {
            return Ok(this._schoolRepository.GetSectionsBySchool(schoolId));
        }

        [Authorize]
        [HttpPost]
        public IHttpActionResult AddStandard([FromBody] Standards standard)
        {
            return Ok(this._schoolRepository.AddStandard(standard));
        }

        [Authorize]
        [HttpGet]
        public IHttpActionResult GetStandardsBySchool(Guid schoolId)
        {
            return Ok(this._schoolRepository.GetStandardsBySchool(schoolId));
        }

        [HttpPost]
        public async Task<IHttpActionResult> DeleteRow([FromBody] Guid schoolAdminId)
        {
            return Ok(await this._schoolRepository.DeleteRow(schoolAdminId));
        }
    }
}