﻿using SMS.BAL.IRepository;
using SMS.DATA;
using System.Web.Http;

namespace SMS.API.Controllers
{
    public class StudentController : ApiController
    {
        private readonly IStudentRepository _studentRepository;
        public StudentController(IStudentRepository studentRepository)
        {
            this._studentRepository = studentRepository;
        }

        [Authorize]
        [HttpPost]
        public IHttpActionResult AddStudent([FromBody] Student student)
        {
            return Ok(this._studentRepository.AddStudent(student));
        }

       
        [HttpGet]
        public IHttpActionResult GetReligions()
        {
            return Ok(this._studentRepository.GetReligion());
        }
    }
}