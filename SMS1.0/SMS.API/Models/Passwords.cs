﻿namespace SMS.API.Models
{
    public class Passwords
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}