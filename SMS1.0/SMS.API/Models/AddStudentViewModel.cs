﻿using SMS.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMS.API.Models
{
    public class AddStudentViewModel
    {
        public SchoolAdmin SchoolAdmin { get; set; }
        public Guid LoggedInSuperAdminId { get; set; }
    }
}