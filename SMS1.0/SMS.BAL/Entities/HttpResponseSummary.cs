﻿using System.Collections.Generic;

namespace SMS.BAL.Entities
{
    public class HttpResponseSummary<T>
    {
        public bool IsSuccess { get; set; }
        public List<string> Messages { get; set; } = new List<string>();
        public T Result { get; set; }
    }
}
