﻿namespace SMS.BAL.Entities
{
    public class DashboardCounts
    {
        public int StudentCount { get; set; }
        public int StaffCount { get; set; }
        public int AttendancePercentage { get; set; }
        public int NewAdmissionCount { get; set; }
    }
}
