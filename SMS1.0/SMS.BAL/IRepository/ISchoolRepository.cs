﻿using SMS.BAL.Entities;
using SMS.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMS.BAL.IRepository
{
    public interface ISchoolRepository
    {
        Task<HttpResponseSummary<bool>> AddSchoolAdmin(SchoolAdmin schoolAdmin, Guid loggedInSuperAdminId);
        Task<HttpResponseSummary<List<SchoolAdmin>>> GetUsers(Guid schoolId);
        Task<List<SchoolAdmin>> SearchUsers(string searchCriterias);
        Task<HttpResponseSummary<bool>> AddSection(Section section);
        Task<HttpResponseSummary<bool>> AddStandard(Standards standard);
        Task<HttpResponseSummary<List<Section>>> GetSectionsBySchool(Guid schoolId);
        Task<HttpResponseSummary<List<Standards>>> GetStandardsBySchool(Guid schoolId);
        Task<HttpResponseSummary<bool>> DeleteRow(Guid schoolAdminId);
    }
}
