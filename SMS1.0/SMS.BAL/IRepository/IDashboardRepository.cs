﻿using SMS.BAL.Entities;
using SMS.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMS.BAL.IRepository
{
    public interface IDashboardRepository
    {
        DashboardCounts GetDashboardCount(Guid schoolId);

    }
}
