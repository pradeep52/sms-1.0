﻿using SMS.BAL.Entities;
using SMS.DATA;
using System.Threading.Tasks;

namespace SMS.BAL.IRepository
{
    public interface ILoginRepository
    {
        Task<HttpResponseSummary<SchoolAdmin>> Login(string userName, string password);
        Task<HttpResponseSummary<string>> ResetPassword(string email);
        Task<HttpResponseSummary<string>> ChangePassword(string oldPassword, string newPassword);
    }
}
