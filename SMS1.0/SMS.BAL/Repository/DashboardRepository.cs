﻿namespace SMS.BAL.Repository
{
    using IRepository;
    using System;
    using System.Linq;
    using Entities;
    using DATA;
    using System.Threading.Tasks;
    using System.Collections.Generic;

    public class DashboardRepository : IDashboardRepository, IDisposable
    {
        private smsDBContext _dbContext = new smsDBContext();
        public DashboardRepository()
        {
            this._dbContext.Configuration.ProxyCreationEnabled = false;
        }
        public DashboardCounts GetDashboardCount(Guid schoolId)
        {
            int studentCount = this._dbContext.Student.Where(s => s.SchoolId == schoolId).Count();
            DashboardCounts dashboardCounts = new DashboardCounts();
            dashboardCounts.StudentCount = studentCount;

            return dashboardCounts;
        }        
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_dbContext != null)
                {
                    _dbContext.Dispose();
                    _dbContext = null;
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
