﻿using SMS.BAL.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SMS.DATA;
using SMS.BAL.Entities;
using SMS.BAL.Helpers;

namespace SMS.BAL.Repository
{
    public class SchoolRepository : ISchoolRepository, IDisposable
    {
        private smsDBContext _dbContext = new smsDBContext();

        public SchoolRepository()
        {
            this._dbContext.Configuration.ProxyCreationEnabled = false;
        }
        public Task<HttpResponseSummary<bool>> AddSchoolAdmin(SchoolAdmin schoolAdmin, Guid loggedInSuperAdminId)
        {
            if (schoolAdmin != null)
            {
                schoolAdmin.Password = Helper.PasswordEncryption(schoolAdmin.Password);
                schoolAdmin.SchoolAdminId = Guid.NewGuid();
                schoolAdmin.CreatedOn = DateTime.Now;
                schoolAdmin.CreatedBy = loggedInSuperAdminId;
                schoolAdmin.IsActive = true;
                using (var transaction = this._dbContext.Database.BeginTransaction())
                {
                    this._dbContext.SchoolAdmin.Add(schoolAdmin);
                    this._dbContext.SaveChanges();
                    transaction.Commit();
                    return Task.FromResult(new HttpResponseSummary<bool>() { IsSuccess = true, Result = true });
                }
            }
            else
            {
                return Task.FromResult(new HttpResponseSummary<bool>() { IsSuccess = false, Messages = new List<string>() { "Unable to create User" }, Result = false });
            }
        }
        public Task<HttpResponseSummary<List<SchoolAdmin>>> GetUsers(Guid schoolId)
        {
            List<SchoolAdmin> users = _dbContext.SchoolAdmin.Where(x => x.SchoolId == schoolId && x.IsActive).ToList();
            return Task.FromResult(new HttpResponseSummary<List<SchoolAdmin>>() { IsSuccess = true, Result = users });
        }
        public Task<List<SchoolAdmin>> SearchUsers(string searchKey)
        {
            var searchArray = searchKey.Split(' ');
            var userList = this._dbContext.SchoolAdmin.Where(x => x.FirstName.ToLower().Contains(searchArray[0]));
            if (searchArray.Count() > 1)
            {
                userList.Where(y => y.LastName.ToLower().Contains(searchArray[1]));
            }
            return Task.FromResult(userList.ToList());
        }
        public Task<HttpResponseSummary<bool>> AddSection(Section section)
        {
            using (var transaction = this._dbContext.Database.BeginTransaction())
            {
                if (!this._dbContext.Section.Any(x => x.Name == section.Name && x.SchoolId == section.SchoolId))
                {
                    this._dbContext.Section.Add(section);
                    this._dbContext.SaveChanges();
                    transaction.Commit();
                    return Task.FromResult(new HttpResponseSummary<bool>() { IsSuccess = true, Result = true });
                }
                else
                {
                    return Task.FromResult(new HttpResponseSummary<bool>() { IsSuccess = false, Messages = new List<string>() { "Internal error occured while adding Section" }, Result = false });
                }

            }
        }
        public Task<HttpResponseSummary<bool>> AddStandard(Standards standard)
        {
            using (var transaction = this._dbContext.Database.BeginTransaction())
            {
                if (!this._dbContext.Standard.Any(x => x.Name == standard.Name && x.SchoolId == standard.SchoolId))
                {
                    this._dbContext.Standard.Add(standard);
                    this._dbContext.SaveChanges();
                    transaction.Commit();
                    return Task.FromResult(new HttpResponseSummary<bool>() { IsSuccess = true, Result = true });
                }
                else
                {
                    return Task.FromResult(new HttpResponseSummary<bool>() { IsSuccess = false, Messages = new List<string> { " Standard with the same name already exists. " }, Result = false });
                }

            }
        }
        public Task<HttpResponseSummary<List<Section>>> GetSectionsBySchool(Guid schoolId)
        {
            List<Section> sectionList = this._dbContext.Section.Where(x => x.SchoolId == schoolId).ToList();
            return Task.FromResult(new HttpResponseSummary<List<Section>>() { IsSuccess = true, Result = sectionList });
        }
        public Task<HttpResponseSummary<List<Standards>>> GetStandardsBySchool(Guid schoolId)
        {
            List<Standards> standardList = this._dbContext.Standard.Where(x => x.SchoolId == schoolId).OrderBy(y => y.Name).ToList();
            return Task.FromResult(new HttpResponseSummary<List<Standards>>() { IsSuccess = true, Result = standardList });
        }

        public async Task<HttpResponseSummary<bool>> DeleteRow(Guid schoolAdminId)
        {
            SchoolAdmin schoolAdmin = _dbContext.SchoolAdmin.FirstOrDefault(u => u.SchoolAdminId == schoolAdminId);

            if (schoolAdmin != null)
            {
                schoolAdmin.RemovedOn = DateTime.Now;
                schoolAdmin.IsActive = false;
                using (var transaction = this._dbContext.Database.BeginTransaction())
                {
                    //this._dbContext.SchoolAdmin.Remove(schoolAdmin);
                    this._dbContext.SaveChanges();
                    transaction.Commit();
                    return await Task.FromResult(new HttpResponseSummary<bool>() { IsSuccess = true, Result = true });
                }
            }
            else
            {
                return await Task.FromResult(new HttpResponseSummary<bool>()
                {
                    IsSuccess = false,
                    Messages = new List<string> { "Record cannot be deleted" },
                    Result = false
                });
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_dbContext != null)
                {
                    _dbContext.Dispose();
                    _dbContext = null;
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
