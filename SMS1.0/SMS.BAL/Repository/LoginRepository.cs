﻿using SMS.BAL.Entities;
using SMS.BAL.Helpers;
using SMS.BAL.IRepository;
using SMS.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SMS.BAL.Repository
{
    public class LoginRepository : IDisposable, ILoginRepository
    {
        private smsDBContext _dbContext = new smsDBContext();
        public LoginRepository()
        {
            this._dbContext.Configuration.ProxyCreationEnabled = false;
        }
        public async Task<HttpResponseSummary<SchoolAdmin>> Login(string userName, string password)
        {
            string ecryptedPassword = Helper.PasswordEncryption(password);
            SchoolAdmin schoolAdmin = _dbContext.SchoolAdmin.FirstOrDefault(u => u.UserName == userName && u.Password == ecryptedPassword && u.IsActive);
            if (schoolAdmin != null)
            {
                return await Task.FromResult(new HttpResponseSummary<SchoolAdmin>()
                {
                    IsSuccess = true,
                    Result = schoolAdmin
                });
            }
            else
            {
                return await Task.FromResult(new HttpResponseSummary<SchoolAdmin>()
                { 
                    IsSuccess = false,
                    Messages = new List<string> { "Invalid Username/Password combination" },
                    Result = null
                });
            }
        }

        public async Task<HttpResponseSummary<string>> ResetPassword(string email)
        {
            SchoolAdmin schoolAdmin = _dbContext.SchoolAdmin.FirstOrDefault(u => u.Email == email && u.IsActive);

            if (schoolAdmin != null)
            {
                schoolAdmin.IsForgotPassword = true;
                schoolAdmin.Password = Helpers.Helper.RandomString(6);
                using (var transaction = this._dbContext.Database.BeginTransaction())
                {
                    this._dbContext.SaveChanges();
                    transaction.Commit();
                    return await Task.FromResult(new HttpResponseSummary<string>() { IsSuccess = true, Result = schoolAdmin.Password });
                }
            }
            else
            {
                return await Task.FromResult(new HttpResponseSummary<string>()
                {
                    IsSuccess = false,
                    Messages = new List<string> { "Invalid EmailId" },
                    Result = null
                });
            }
        }

        public async Task<HttpResponseSummary<string>> ChangePassword(string oldPassword, string newPassword)
        {
            SchoolAdmin schoolAdmin = _dbContext.SchoolAdmin.FirstOrDefault(u => u.Password == oldPassword);

            if (schoolAdmin != null)
            {
                schoolAdmin.Password = Helper.PasswordEncryption(newPassword);
                schoolAdmin.IsForgotPassword = false;
                using (var transaction = this._dbContext.Database.BeginTransaction())
                {
                    this._dbContext.SaveChanges();
                    transaction.Commit();
                    return await Task.FromResult(new HttpResponseSummary<string>() { IsSuccess = true, Result = schoolAdmin.Password });
                }
            }
            else
            {
                return await Task.FromResult(new HttpResponseSummary<string>()
                {
                    IsSuccess = false,
                    Messages = new List<string> { "Invalid Old Password" },
                    Result = null
                });
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_dbContext != null)
                {
                    _dbContext.Dispose();
                    _dbContext = null;
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
