﻿using SMS.BAL.Entities;
using SMS.BAL.IRepository;
using SMS.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SMS.BAL.Repository
{
    public class StudentRepository : IStudentRepository, IDisposable
    {
        private smsDBContext _dbContext = new smsDBContext();

        public StudentRepository()
        {
            this._dbContext.Configuration.ProxyCreationEnabled = false;
        }


        public async Task<HttpResponseSummary<List<Religion>>> GetReligion()
        {
            List<Religion> religions = this._dbContext.Religion.ToList();
            return await Task.FromResult(new HttpResponseSummary<List<Religion>>() { Result = religions });
        }

        public async Task<HttpResponseSummary<Student>> AddStudent(Student student)
        {
            student.Address = await this.AddAddressForStudent(student.Address);
            student.Guardian = await this.AddGuardianForStudent(student.Guardian);

            // Add student record to db
            using (var transaction = this._dbContext.Database.BeginTransaction())
            {
                if (!this._dbContext.Student.Any(x => x.Address == student.Address && x.Guardian == student.Guardian))
                {
                    this._dbContext.Student.Add(student);
                    this._dbContext.SaveChanges();
                    transaction.Commit();
                    return await Task.FromResult(new HttpResponseSummary<Student>() { IsSuccess = true, Result = student });
                }
                else
                {
                    return await Task.FromResult(new HttpResponseSummary<Student>() { IsSuccess = false, Messages = new List<string>() { "Internal error occured while adding Student" }, Result = null });
                }

            }
        }

        private async Task<Guardian> AddGuardianForStudent(Guardian guardian)
        {
            using (var transaction = this._dbContext.Database.BeginTransaction())
            {
                this._dbContext.Guardian.Add(guardian);
                this._dbContext.SaveChanges();
                transaction.Commit();
                return await Task.FromResult(guardian);
            }
        }

        private async Task<Address> AddAddressForStudent(Address address)
        {
            using (var transaction = this._dbContext.Database.BeginTransaction())
            {
                this._dbContext.Address.Add(address);
                this._dbContext.SaveChanges();
                transaction.Commit();
                return await Task.FromResult(address);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_dbContext != null)
                {
                    _dbContext.Dispose();
                    _dbContext = null;
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        
    }
}
