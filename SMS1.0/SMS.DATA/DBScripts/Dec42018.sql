
CREATE TABLE [dbo].[Address](
	[AddressId] [uniqueidentifier] NOT NULL,
	[Country] [varchar](20) NOT NULL,
	[State] [varchar](20) NOT NULL,
	[City] [varchar](20) NOT NULL,
	[Description] [text] NOT NULL,
	[PinCode] [varchar](10) NULL,
	[LandMark] [text] NULL,
 CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED 
(
	[AddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Caste]    Script Date: 04-12-2018 17:22:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Caste](
	[CasteId] [uniqueidentifier] NOT NULL,
	[ReligionId] [uniqueidentifier] NOT NULL,
	[CasteName] [varchar](100) NOT NULL,
	[SubCasteName] [varchar](100) NULL,
	[Category] [varchar](10) NOT NULL,
 CONSTRAINT [PK_Caste] PRIMARY KEY CLUSTERED 
(
	[CasteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Guardian]    Script Date: 04-12-2018 17:22:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Guardian](
	[GuardianId] [uniqueidentifier] NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[MiddleName] [varchar](50) NULL,
	[LastName] [varchar](50) NOT NULL,
	[Gender] [varchar](1) NOT NULL,
	[RelationWithStudent] [varchar](20) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[PrimaryContactNumber] [varchar](20) NOT NULL,
	[SecondaryContactNumber] [varchar](50) NULL,
	[Occupation] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Guardian] PRIMARY KEY CLUSTERED 
(
	[GuardianId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Religion]    Script Date: 04-12-2018 17:22:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Religion](
	[ReligionId] [uniqueidentifier] NOT NULL,
	[ReligionName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Religion] PRIMARY KEY CLUSTERED 
(
	[ReligionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[School]    Script Date: 04-12-2018 17:22:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[School](
	[SchoolId] [uniqueidentifier] NOT NULL,
	[Name] [varchar](max) NOT NULL,
	[Address] [text] NOT NULL,
	[Country] [varchar](50) NOT NULL,
	[State] [varchar](50) NOT NULL,
	[City] [varchar](50) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[PrimaryContactNo] [varchar](20) NOT NULL,
	[SecondaryContactNo] [varchar](20) NULL,
 CONSTRAINT [PK_School] PRIMARY KEY CLUSTERED 
(
	[SchoolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SchoolAdmin]    Script Date: 04-12-2018 17:22:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SchoolAdmin](
	[SchoolAdminId] [uniqueidentifier] NOT NULL,
	[SchoolId] [uniqueidentifier] NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[MiddleName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[Email] [varchar](50) NULL,
	[UserName] [varchar](50) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[PrimaryContactNo] [varchar](20) NOT NULL,
	[SecondaryContactNo] [varchar](20) NULL,
	[IsSchoolSuperAdmin] [bit] NOT NULL,
	[IsForgotPassword] [bit] NOT NULL,
 CONSTRAINT [PK_SchoolAdmin] PRIMARY KEY CLUSTERED 
(
	[SchoolAdminId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Section]    Script Date: 04-12-2018 17:22:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Section](
	[SectionId] [uniqueidentifier] NOT NULL,
	[SchoolId] [uniqueidentifier] NOT NULL,
	[Name] [varchar](2) NOT NULL,
 CONSTRAINT [PK_Section] PRIMARY KEY CLUSTERED 
(
	[SectionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Standards]    Script Date: 04-12-2018 17:22:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Standards](
	[StandardId] [uniqueidentifier] NOT NULL,
	[SchoolId] [uniqueidentifier] NOT NULL,
	[Name] [varchar](50) NULL,
 CONSTRAINT [PK_Standard] PRIMARY KEY CLUSTERED 
(
	[StandardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Student]    Script Date: 04-12-2018 17:22:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Student](
	[StudentId] [uniqueidentifier] NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[MiddleName] [varchar](50) NULL,
	[LastName] [varchar](50) NOT NULL,
	[DOB] [date] NOT NULL,
	[Gender] [varchar](1) NOT NULL,
	[RollNumber] [varchar](10) NULL,
	[SchoolId] [uniqueidentifier] NOT NULL,
	[GuardianId] [uniqueidentifier] NOT NULL,
	[AddressId] [uniqueidentifier] NOT NULL,
	[CasteId] [uniqueidentifier] NOT NULL,
	[StandardId] [uniqueidentifier] NOT NULL,
	[SectionId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Student] PRIMARY KEY CLUSTERED 
(
	[StudentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Section] ADD  CONSTRAINT [DF_Section_SectionId]  DEFAULT (newid()) FOR [SectionId]
GO
ALTER TABLE [dbo].[Standards] ADD  CONSTRAINT [DF_Standards_StandardId]  DEFAULT (newid()) FOR [StandardId]
GO
ALTER TABLE [dbo].[Caste]  WITH CHECK ADD  CONSTRAINT [FK_Caste_Religion] FOREIGN KEY([ReligionId])
REFERENCES [dbo].[Religion] ([ReligionId])
GO
ALTER TABLE [dbo].[Caste] CHECK CONSTRAINT [FK_Caste_Religion]
GO
ALTER TABLE [dbo].[SchoolAdmin]  WITH CHECK ADD  CONSTRAINT [FK_SchoolAdmin_School] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[School] ([SchoolId])
GO
ALTER TABLE [dbo].[SchoolAdmin] CHECK CONSTRAINT [FK_SchoolAdmin_School]
GO
ALTER TABLE [dbo].[Section]  WITH CHECK ADD  CONSTRAINT [Section_SchoolId_FK] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[School] ([SchoolId])
GO
ALTER TABLE [dbo].[Section] CHECK CONSTRAINT [Section_SchoolId_FK]
GO
ALTER TABLE [dbo].[Standards]  WITH CHECK ADD  CONSTRAINT [Standard_SchoolId_FK] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[School] ([SchoolId])
GO
ALTER TABLE [dbo].[Standards] CHECK CONSTRAINT [Standard_SchoolId_FK]
GO
ALTER TABLE [dbo].[Student]  WITH CHECK ADD  CONSTRAINT [Student_AddressId_FK] FOREIGN KEY([AddressId])
REFERENCES [dbo].[Address] ([AddressId])
GO
ALTER TABLE [dbo].[Student] CHECK CONSTRAINT [Student_AddressId_FK]
GO
ALTER TABLE [dbo].[Student]  WITH CHECK ADD  CONSTRAINT [Student_GuardianId_FK] FOREIGN KEY([GuardianId])
REFERENCES [dbo].[Guardian] ([GuardianId])
GO
ALTER TABLE [dbo].[Student] CHECK CONSTRAINT [Student_GuardianId_FK]
GO
ALTER TABLE [dbo].[Student]  WITH CHECK ADD  CONSTRAINT [Student_SchoolId_FK] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[School] ([SchoolId])
GO
ALTER TABLE [dbo].[Student] CHECK CONSTRAINT [Student_SchoolId_FK]
GO
ALTER TABLE [dbo].[Student]  WITH CHECK ADD  CONSTRAINT [Student_SectionId_FK] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[Student] CHECK CONSTRAINT [Student_SectionId_FK]
GO
ALTER TABLE [dbo].[Student]  WITH CHECK ADD  CONSTRAINT [Student_StandardId_FK] FOREIGN KEY([StandardId])
REFERENCES [dbo].[Standards] ([StandardId])
GO
ALTER TABLE [dbo].[Student] CHECK CONSTRAINT [Student_StandardId_FK]
GO
USE [master]
GO
ALTER DATABASE [smsDB] SET  READ_WRITE 
GO
