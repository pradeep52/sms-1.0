USE [master]
GO
/****** Object:  Database [smsDB]    Script Date: 14-12-2018 16:11:54 ******/
CREATE DATABASE [smsDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'smsDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\smsDB.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'smsDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\smsDB_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [smsDB] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [smsDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [smsDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [smsDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [smsDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [smsDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [smsDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [smsDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [smsDB] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [smsDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [smsDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [smsDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [smsDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [smsDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [smsDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [smsDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [smsDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [smsDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [smsDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [smsDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [smsDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [smsDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [smsDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [smsDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [smsDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [smsDB] SET RECOVERY FULL 
GO
ALTER DATABASE [smsDB] SET  MULTI_USER 
GO
ALTER DATABASE [smsDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [smsDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [smsDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [smsDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'smsDB', N'ON'
GO
USE [smsDB]
GO
/****** Object:  Table [dbo].[Address]    Script Date: 14-12-2018 16:11:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Address](
	[AddressId] [uniqueidentifier] NOT NULL,
	[Country] [varchar](20) NOT NULL,
	[State] [varchar](20) NOT NULL,
	[City] [varchar](20) NOT NULL,
	[Description] [text] NOT NULL,
	[PinCode] [varchar](10) NULL,
	[LandMark] [text] NULL,
 CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED 
(
	[AddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Caste]    Script Date: 14-12-2018 16:11:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Caste](
	[CasteId] [uniqueidentifier] NOT NULL,
	[ReligionId] [uniqueidentifier] NOT NULL,
	[CasteName] [varchar](100) NOT NULL,
	[SubCasteName] [varchar](100) NULL,
	[Category] [varchar](10) NOT NULL,
 CONSTRAINT [PK_Caste] PRIMARY KEY CLUSTERED 
(
	[CasteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Guardian]    Script Date: 14-12-2018 16:11:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Guardian](
	[GuardianId] [uniqueidentifier] NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[MiddleName] [varchar](50) NULL,
	[LastName] [varchar](50) NOT NULL,
	[Gender] [varchar](1) NOT NULL,
	[RelationWithStudent] [varchar](20) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[PrimaryContactNumber] [varchar](20) NOT NULL,
	[SecondaryContactNumber] [varchar](50) NULL,
	[Occupation] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Guardian] PRIMARY KEY CLUSTERED 
(
	[GuardianId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Religion]    Script Date: 14-12-2018 16:11:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Religion](
	[ReligionId] [uniqueidentifier] NOT NULL,
	[ReligionName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Religion] PRIMARY KEY CLUSTERED 
(
	[ReligionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[School]    Script Date: 14-12-2018 16:11:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[School](
	[SchoolId] [uniqueidentifier] NOT NULL,
	[Name] [varchar](max) NOT NULL,
	[Address] [text] NOT NULL,
	[Country] [varchar](50) NOT NULL,
	[State] [varchar](50) NOT NULL,
	[City] [varchar](50) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[PrimaryContactNo] [varchar](20) NOT NULL,
	[SecondaryContactNo] [varchar](20) NULL,
 CONSTRAINT [PK_School] PRIMARY KEY CLUSTERED 
(
	[SchoolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SchoolAdmin]    Script Date: 14-12-2018 16:11:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SchoolAdmin](
	[SchoolAdminId] [uniqueidentifier] NOT NULL,
	[SchoolId] [uniqueidentifier] NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[MiddleName] [varchar](50) NULL,
	[LastName] [varchar](50) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[PrimaryContactNo] [varchar](20) NOT NULL,
	[SecondaryContactNo] [varchar](20) NULL,
	[IsSchoolSuperAdmin] [bit] NOT NULL,
	[IsForgotPassword] [bit] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[RemovedOn] [datetimeoffset](7) NULL,
	[CreatedBy] [uniqueidentifier] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_SchoolAdmin] PRIMARY KEY CLUSTERED 
(
	[SchoolAdminId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Section]    Script Date: 14-12-2018 16:11:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Section](
	[SectionId] [uniqueidentifier] NOT NULL,
	[SchoolId] [uniqueidentifier] NOT NULL,
	[Name] [varchar](2) NOT NULL,
 CONSTRAINT [PK_Section] PRIMARY KEY CLUSTERED 
(
	[SectionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Standards]    Script Date: 14-12-2018 16:11:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Standards](
	[StandardId] [uniqueidentifier] NOT NULL,
	[SchoolId] [uniqueidentifier] NOT NULL,
	[Name] [varchar](50) NULL,
 CONSTRAINT [PK_Standard] PRIMARY KEY CLUSTERED 
(
	[StandardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Student]    Script Date: 14-12-2018 16:11:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Student](
	[StudentId] [uniqueidentifier] NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[MiddleName] [varchar](50) NULL,
	[LastName] [varchar](50) NOT NULL,
	[DOB] [date] NOT NULL,
	[Gender] [varchar](1) NOT NULL,
	[RollNumber] [varchar](10) NULL,
	[SchoolId] [uniqueidentifier] NOT NULL,
	[GuardianId] [uniqueidentifier] NOT NULL,
	[AddressId] [uniqueidentifier] NOT NULL,
	[CasteId] [uniqueidentifier] NOT NULL,
	[StandardId] [uniqueidentifier] NOT NULL,
	[SectionId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Student] PRIMARY KEY CLUSTERED 
(
	[StudentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Section] ADD  CONSTRAINT [DF_Section_SectionId]  DEFAULT (newid()) FOR [SectionId]
GO
ALTER TABLE [dbo].[Standards] ADD  CONSTRAINT [DF_Standards_StandardId]  DEFAULT (newid()) FOR [StandardId]
GO
ALTER TABLE [dbo].[Caste]  WITH CHECK ADD  CONSTRAINT [FK_Caste_Religion] FOREIGN KEY([ReligionId])
REFERENCES [dbo].[Religion] ([ReligionId])
GO
ALTER TABLE [dbo].[Caste] CHECK CONSTRAINT [FK_Caste_Religion]
GO
ALTER TABLE [dbo].[SchoolAdmin]  WITH CHECK ADD  CONSTRAINT [FK_SchoolAdmin_School] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[School] ([SchoolId])
GO
ALTER TABLE [dbo].[SchoolAdmin] CHECK CONSTRAINT [FK_SchoolAdmin_School]
GO
ALTER TABLE [dbo].[Section]  WITH CHECK ADD  CONSTRAINT [Section_SchoolId_FK] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[School] ([SchoolId])
GO
ALTER TABLE [dbo].[Section] CHECK CONSTRAINT [Section_SchoolId_FK]
GO
ALTER TABLE [dbo].[Standards]  WITH CHECK ADD  CONSTRAINT [Standard_SchoolId_FK] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[School] ([SchoolId])
GO
ALTER TABLE [dbo].[Standards] CHECK CONSTRAINT [Standard_SchoolId_FK]
GO
ALTER TABLE [dbo].[Student]  WITH CHECK ADD  CONSTRAINT [Student_AddressId_FK] FOREIGN KEY([AddressId])
REFERENCES [dbo].[Address] ([AddressId])
GO
ALTER TABLE [dbo].[Student] CHECK CONSTRAINT [Student_AddressId_FK]
GO
ALTER TABLE [dbo].[Student]  WITH CHECK ADD  CONSTRAINT [Student_GuardianId_FK] FOREIGN KEY([GuardianId])
REFERENCES [dbo].[Guardian] ([GuardianId])
GO
ALTER TABLE [dbo].[Student] CHECK CONSTRAINT [Student_GuardianId_FK]
GO
ALTER TABLE [dbo].[Student]  WITH CHECK ADD  CONSTRAINT [Student_SchoolId_FK] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[School] ([SchoolId])
GO
ALTER TABLE [dbo].[Student] CHECK CONSTRAINT [Student_SchoolId_FK]
GO
ALTER TABLE [dbo].[Student]  WITH CHECK ADD  CONSTRAINT [Student_SectionId_FK] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[Student] CHECK CONSTRAINT [Student_SectionId_FK]
GO
ALTER TABLE [dbo].[Student]  WITH CHECK ADD  CONSTRAINT [Student_StandardId_FK] FOREIGN KEY([StandardId])
REFERENCES [dbo].[Standards] ([StandardId])
GO
ALTER TABLE [dbo].[Student] CHECK CONSTRAINT [Student_StandardId_FK]
GO
USE [master]
GO
ALTER DATABASE [smsDB] SET  READ_WRITE 
GO
