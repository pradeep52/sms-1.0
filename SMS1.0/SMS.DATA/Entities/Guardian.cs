﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMS.DATA
{
    public class Guardian
    {
        public Guardian()
        {
            this.Students = new HashSet<Student>();
        }
        public Guid GuardianId { get; set; }
        public Guid SchoolId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string RelationWithStudent { get; set; }
        public string Email { get; set; }
        public string PrimaryContactNumber { get; set; }
        public string SecondaryContactNumber { get; set; }
        public string Occupation { get; set; }
        public School School { get; set; }
        public ICollection<Student> Students { get; set; }
    }
}
