﻿using System;

namespace SMS.DATA.Entities
{
    public class HearingHistory
    {
        public int HearingId { get; set; }
        public bool DifficultyObserved { get; set; }
        public bool IsConsulted { get; set; }
        public string Explaination { get; set; }
        public DateTimeOffset ModifiedOn { get; set; }
        public Guid ModifiedBy { get; set; }
    }
}
