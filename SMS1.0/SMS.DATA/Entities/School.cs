namespace SMS.DATA
{
    using System;
    using System.Collections.Generic;

    public class School
    {
        public School()
        {
            SchoolAdmins = new HashSet<SchoolAdmin>();
            Students = new HashSet<Student>();
            Standards = new HashSet<Standards>();
            Sections = new HashSet<Section>();
        }

        public Guid SchoolId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
        public string PrimaryContactNo { get; set; }
        public string SecondaryContactNo { get; set; }
        public virtual ICollection<SchoolAdmin> SchoolAdmins { get; set; }
        public virtual ICollection<Student> Students { get; set; }
        public virtual ICollection<Standards> Standards { get; set; }
        public virtual ICollection<Section> Sections { get; set; }
        public virtual ICollection<Guardian> Guardians{ get; set; }
    }
}
