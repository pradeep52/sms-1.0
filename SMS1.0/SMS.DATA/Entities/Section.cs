﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMS.DATA
{
    public class Section
    {
        public Section()
        {
            Students = new HashSet<Student>();
        }
        public Guid SectionId { get; set; }
        public Guid SchoolId { get; set; }
        public string Name { get; set; }
        public virtual School School { get; set; }
        public virtual ICollection<Student> Students { get; set; }
    }
}
