﻿namespace SMS.DATA
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    public class Student
    {
        public Guid StudentId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public Guid SchoolId { get; set; }
        public Guid GuardianId { get; set; }
        public Guid AddressId { get; set; }
        public Guid CasteId { get; set; }
        public Guid StandardId { get; set; }
        public Guid SectionId { get; set; }
        public virtual School School { get; set; }
        public virtual Section Section { get; set; }
        public virtual Standards Standard { get; set; }
        public virtual Caste Caste { get; set; }
        public virtual Address Address { get; set; }
        public virtual Guardian Guardian { get; set; }

    }
}
