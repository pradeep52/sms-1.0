﻿namespace SMS.DATA
{
    using System;
    using System.Collections.Generic;
    public class Standards
    {
        public Standards()
        {
            Students = new HashSet<Student>();
        }
        public Guid StandardId { get; set; }
        public string Name { get; set; }
        public Guid SchoolId { get; set; }
        public virtual School School { get; set; }
        public virtual ICollection<Student> Students { get; set; }
    }
}
