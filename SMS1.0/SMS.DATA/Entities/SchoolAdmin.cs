namespace SMS.DATA
{
    using System;

    public class SchoolAdmin
    {
        public Guid SchoolAdminId { get; set; }
        public Guid SchoolId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string PrimaryContactNo { get; set; }
        public string SecondaryContactNo { get; set; }
        public bool IsSchoolSuperAdmin { get; set; }
        public virtual School School { get; set; }
        public bool IsForgotPassword { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public DateTimeOffset RemovedOn { get; set; }
        public Guid CreatedBy { get; set; }
        public bool IsActive { get; set; }
    }
}
