﻿using System;
using System.Collections.Generic;

namespace SMS.DATA
{
    public class Caste
    {
        public Caste()
        {
            this.Students = new HashSet<Student>();
        }
        public Guid CasteId { get; set; }
        public Guid ReligionId { get; set; }
        public virtual Religion Religion { get; set; }
        public string CasteName { get; set; }
        public string SubCasteName { get; set; }
        public string Category { get; set; }
        public virtual ICollection<Student> Students { get; set; }
    }
}
