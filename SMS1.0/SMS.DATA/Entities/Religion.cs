﻿using System;
using System.Collections.Generic;

namespace SMS.DATA
{
    public class Religion
    {
        public Guid ReligionId { get; set; }
        public string ReligionName { get; set; }
        public ICollection<Caste> Caste { get; set; }
    }
}
