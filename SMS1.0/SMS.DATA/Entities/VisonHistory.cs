﻿using System;

namespace SMS.DATA.Entities
{
    public class VisonHistory
    {
        public int VisionHistoryId { get; set; }
        public bool IsConsulted { get; set; }
        public bool SpectaclesUsed { get; set; }
        public DateTimeOffset ModifiedOn { get; set; }
        public Guid ModifiedBy { get; set; }
    }
}
