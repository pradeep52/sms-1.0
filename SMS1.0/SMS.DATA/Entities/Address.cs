﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMS.DATA
{
    public class Address
    {
        public Address()
        {
            this.Students = new HashSet<Student>();
        }
        public Guid AddressId { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Description { get; set; }
        public string PinCode { get; set; }
        public string LandMark { get; set; }
        public ICollection<Student> Students { get; set; }
    }
}
