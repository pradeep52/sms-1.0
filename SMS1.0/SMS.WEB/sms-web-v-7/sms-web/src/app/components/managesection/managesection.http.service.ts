import { Injectable } from '@angular/core';
import { HttpService } from '../../helpers/service/http.service';
import { AppConfig } from '../../helpers/constants/app.config';
import { Observable } from 'rxjs';

@Injectable()

export class ManageSectionHttpService {
    constructor(private _httpService: HttpService) { }

    getSectionsBySchool(schoolId: string): Observable<any> {
        return this._httpService.getRequest(AppConfig.API_ENDPOINT_URL + 'schooladmin/getsectionsbyschool?schoolId=' + schoolId);
    }
}
