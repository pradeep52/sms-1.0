import { Component, OnInit } from '@angular/core';
import { Section } from '../../models/section';
import { ManageSectionHttpService } from './managesection.http.service';

@Component({
  selector: 'app-managesection',
  templateUrl: './managesection.component.html',
  styleUrls: ['./managesection.component.css'],
  providers: [ManageSectionHttpService]
})
export class ManagesectionComponent implements OnInit {

  public Sections: Array<Section>[] = [];

  constructor(private _manageSectionHttpService: ManageSectionHttpService) { }

  ngOnInit() {
    this.GetSections();
  }


  private GetSections() {
    var _userInfo = localStorage.getItem('UserInfo');
    let _loggedUser = JSON.parse(_userInfo);
    var res;
    this._manageSectionHttpService.getSectionsBySchool(_loggedUser.SchoolId)
    .subscribe(result => res = result, error => console.log(error), () => this.OnGetSectionsComplete(res));
  }

  OnGetSectionsComplete(res: any) {
    this.Sections = res.Result.Result;
  }

}
