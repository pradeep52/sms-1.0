import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AddSectionHttpService } from './addsection.http.service';
import { Section } from '../../models/section';
import { ValidationSummary } from '../../models/validationsummary';

@Component({
  selector: 'app-addsection',
  templateUrl: './addsection.component.html',
  styleUrls: ['./addsection.component.css'],
  providers: [ AddSectionHttpService ]
})
export class AddsectionComponent implements OnInit {

  public SectionName: string;
  public ValidationSummary: ValidationSummary;

  constructor(private _router: Router, private _addSectionHttpService: AddSectionHttpService) {
    this.ValidationSummary = new ValidationSummary();
  }

  ngOnInit() {
  }

  AddSection() {
    if (this.SectionName) {
      let _section: Section = this.PopulateSection();
      var res;
      this._addSectionHttpService.addSection(_section)
      .subscribe(result => res = result, error => console.log(error), () => this.OnAddSectionComplete());
    } else {
      this.ValidationSummary.IsValid = false;
      this.ValidationSummary.Messages.push('Section is mandatory.');
    }
  }

  private PopulateSection(): Section {
    var _userInfo = localStorage.getItem('UserInfo');
    let _loggedUser = JSON.parse(_userInfo);

    let _section: Section = new Section();
    _section.SchoolId = _loggedUser.SchoolId;
    _section.Name = this.SectionName;

    return _section;
  }

  OnAddSectionComplete(): any {
    this._router.navigate(['/dashboard/manage-section']);
  }

}
