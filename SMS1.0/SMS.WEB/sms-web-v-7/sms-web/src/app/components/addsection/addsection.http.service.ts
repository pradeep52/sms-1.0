import { Injectable } from '@angular/core';
import { HttpService } from '../../helpers/service/http.service';
import { AppConfig } from '../../helpers/constants/app.config';
import { Section } from '../../models/section';
import { Observable } from 'rxjs';


@Injectable()

export class AddSectionHttpService {
    constructor(private _httpService: HttpService) { }

    addSection(section: Section): Observable<any> {
        return this._httpService.postRequest(AppConfig.API_ENDPOINT_URL + 'schooladmin/addsection', JSON.stringify(section));
    }
}
