import { Component, OnInit, ViewChild, Input, EventEmitter, Output } from '@angular/core';
import { NgbActiveModal, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modalconfirm',
  templateUrl: './modalconfirm.component.html',
  styleUrls: ['./modalconfirm.component.css']
})
export class ModalconfirmComponent {
  public ValidationMessage: string;
  @ViewChild("content") content: any;
  @Input() message;
  @Output() closeReasonEvent = new EventEmitter();
  public closeResult: any;
  constructor(public modalService: NgbModal) { }

  public Open(){
    this.modalService.open(this.content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }


  private getDismissReason(reason: any)
   { 
    switch(reason)
    {
      case "Cancel":
      break;
      case "Ok":
        this.closeReasonEvent.emit(reason);
      break;
    }
}
}
