import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ValidationSummary } from '../../models/validationsummary';
import { AddStandardHttpService } from './addstandard.http.service';
import { Standard } from '../../models/standard';

@Component({
  selector: 'app-addstandard',
  templateUrl: './addstandard.component.html',
  styleUrls: ['./addstandard.component.css'],
  providers: [AddStandardHttpService]
})
export class AddstandardComponent implements OnInit {

  public StandardName: string;
  public ValidationSummary: ValidationSummary;

  constructor(private _router: Router, private _addStandardHttpService: AddStandardHttpService)
  {
    this.ValidationSummary = new ValidationSummary();
  }

  public AddStandard() {
    if (this.StandardName) {
      let _standard: Standard = this.populateStandard();
      var res;
      this._addStandardHttpService.addStandard(_standard)
      .subscribe(result => res = result, error => console.log(error), () => this.OnAddStandardComplete(res));
    } else {
      this.ValidationSummary.IsValid = false;
      this.ValidationSummary.Messages.push('Standard is mandatory.');
    }
  }

  private populateStandard(): Standard {
    var _userInfo = localStorage.getItem('UserInfo');
    let _loggedUser = JSON.parse(_userInfo);

    let _stanadard: Standard = new Standard();
    _stanadard.SchoolId = _loggedUser.SchoolId;
    _stanadard.Name = this.StandardName;

    return _stanadard;
  }

  OnAddStandardComplete(res: any) {
    if (res.Result.IsSuccess) {
      this._router.navigate(['/dashboard/manage-standard']);
     } else {
       this.ValidationSummary.Messages = res.Result.Messages;
     }
  }

  ngOnInit() {
  }

}
