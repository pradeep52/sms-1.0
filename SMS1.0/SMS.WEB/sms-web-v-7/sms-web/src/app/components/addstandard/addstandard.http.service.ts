import { Injectable } from '@angular/core';
import { HttpService } from '../../helpers/service/http.service';
import { AppConfig } from '../../helpers/constants/app.config';
import { Standard } from '../../models/standard';
import { Observable } from 'rxjs';


@Injectable()

export class AddStandardHttpService {
    constructor(private _httpService: HttpService) { }

    addStandard(standard: Standard): Observable<any> {
        return this._httpService.postRequest(AppConfig.API_ENDPOINT_URL + 'schooladmin/addstandard', JSON.stringify(standard));
    }
}
