import { Component, OnInit } from '@angular/core';
import { ManageStandardHttpService } from './managestandard.http.service';
import { Standard } from '../../models/standard';

@Component({
  selector: 'app-managestandard',
  templateUrl: './managestandard.component.html',
  styleUrls: ['./managestandard.component.css'],
  providers : [ManageStandardHttpService]
})
export class ManagestandardComponent implements OnInit {

  public Standards: Array<Standard>;

  constructor(private _manageStandardHttpService: ManageStandardHttpService) { }

  ngOnInit() {
    this.GetStandards();
  }

  private GetStandards() {
    var _userInfo = localStorage.getItem('UserInfo');
    let _loggedUser = JSON.parse(_userInfo);
    var res;
    this._manageStandardHttpService.getStandardsBySchool(_loggedUser.SchoolId)
    .subscribe(result => res = result, error => console.log(error), () => this.OnGetStandardComplete(res));
  }

  OnGetStandardComplete(res: any) {
    if (res.Result.IsSuccess) {
      this.Standards = res.Result.Result;
    }
  }

}
