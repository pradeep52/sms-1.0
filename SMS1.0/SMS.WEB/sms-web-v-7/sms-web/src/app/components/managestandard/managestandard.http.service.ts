import { Injectable, Component } from '@angular/core';
import { HttpService } from '../../helpers/service/http.service';
import { AppConfig } from '../../helpers/constants/app.config';
import { Observable } from 'rxjs';

@Injectable()

export class ManageStandardHttpService {
    constructor(private _httpService: HttpService) { }

    getStandardsBySchool(schoolId: string): Observable<any> {
        return this._httpService.getRequest(AppConfig.API_ENDPOINT_URL + 'schooladmin/getstandardsbyschool?schoolId=' + schoolId);
    }
}
