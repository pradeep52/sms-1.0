import { Injectable,Component } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpService } from '../../helpers/service/http.service';
import { AppConfig } from '../../helpers/constants/app.config';


@Injectable()

export class LoginService
{
    constructor(private _http: HttpClient, private _httpService:HttpService){ }

    login(credentials:any):Observable<any>{
        return this._httpService.postRequest(AppConfig.API_ENDPOINT_URL + 'login/login',JSON.stringify(credentials));
    }

    authenticate(credentials:any):Observable<any>{
        var headers = new HttpHeaders({'Content-Type':'application/json'});
        var data = "username=" + credentials.UserName + "&password=" + credentials.Password + "&grant_type=password";
        return this._http.post(AppConfig.API_TOKEN_URL + '/token',data,{headers:headers});
    }
}