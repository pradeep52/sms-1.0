import { Injectable,Component } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from '../../helpers/service/http.service';
import { AppConfig } from '../../helpers/constants/app.config';
import { School } from '../../models/school';

@Injectable()

export class UserListHttpService
{
    constructor(private _httpService: HttpService) { }

    getUsers(schoolId: string): Observable<any> {
        return this._httpService.getRequest(AppConfig.API_ENDPOINT_URL + 'schooladmin/getusers?schoolId=' + schoolId);
    }

    deleteUser(schoolAdmninId: string): Observable<any>{
        return this._httpService.postRequest(AppConfig.API_ENDPOINT_URL + 'schooladmin/deleterow',  JSON.stringify(schoolAdmninId));
    }
}
