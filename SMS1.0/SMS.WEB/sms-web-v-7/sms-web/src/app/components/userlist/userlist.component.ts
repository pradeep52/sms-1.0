import { Component, OnInit, ViewChild } from '@angular/core';
import { UserListHttpService } from '../../components/userlist/userlist.service';
import { SchoolAdmin } from '../../models/schooladmin';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef,ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ModalconfirmComponent } from '../usercontrols/modalconfirm/modalconfirm.component';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css'],
  providers: [UserListHttpService]
})
export class UserlistComponent implements OnInit {
  public Users: Array<SchoolAdmin>[]= [];
  public ModalRef: NgbModalRef;
  public ModalMessage: string;
  @ViewChild('confirmationModal') confirmationModal: ModalconfirmComponent;

  private userToDelete: SchoolAdmin;
  constructor(private _router: Router, private userListService: UserListHttpService, private modalService: NgbModal) { }

  ngOnInit() {
    this.GetUsers();
  }
  public OpenConfirmDialogueBox(user:SchoolAdmin)
  {
    this.userToDelete = user;
    this.ModalMessage = "Are you sure want to delete " + user.FirstName + " " + user.LastName + "?";
    this.confirmationModal.Open();
  }

  private GetUsers()
  {
    var _userInfo = localStorage.getItem('UserInfo');
    let _loggedUser = JSON.parse(_userInfo);
    var res;
    this.userListService.getUsers(_loggedUser.SchoolId)
    .subscribe(result => res = result, error => console.log(error), () => this.OnGetUsersComplete(res));
  }

  private OnGetUsersComplete(userListResult: any)
  {
    this.Users = userListResult.Result.Result;
  }

  public getConfirmationResult(result: any)
  {
    this.userListService.deleteUser(this.userToDelete.SchoolAdminId)
    .subscribe(result =>{}, error => console.log(error), () => this.GetUsers()); 
  }

}
