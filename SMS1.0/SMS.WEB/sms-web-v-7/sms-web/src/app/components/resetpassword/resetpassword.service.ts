import { Injectable,Component } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from '../../helpers/service/http.service';
import { AppConfig } from '../../helpers/constants/app.config';
import { ValidationSummary } from '../../models/validationsummary';


@Injectable()

export class ResetPasswordService
{
    constructor(private _httpService:HttpService){ }

    changepassword(changedpassword:any):Observable<any>{
        return this._httpService.postRequest(AppConfig.API_ENDPOINT_URL + 'login/changepassword',JSON.stringify(changedpassword));
    }

    validatefields(matchedpassword:any):ValidationSummary{
        let validationSummary: ValidationSummary = new ValidationSummary();
        if (matchedpassword.NewPassword !== matchedpassword.ConfirmPassword) {
                validationSummary.IsValid = false;
                validationSummary.Messages.push('Passwords do not match');
        }
        return validationSummary;
    }
}