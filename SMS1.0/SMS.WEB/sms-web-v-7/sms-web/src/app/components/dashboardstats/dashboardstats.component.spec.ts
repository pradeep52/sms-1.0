import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardstatsComponent } from './dashboardstats.component';

describe('DashboardstatsComponent', () => {
  let component: DashboardstatsComponent;
  let fixture: ComponentFixture<DashboardstatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardstatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardstatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
