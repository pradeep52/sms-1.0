import { Observable } from 'rxjs';
import { HttpService } from '../../helpers/service/http.service';
import { AppConfig } from '../../helpers/constants/app.config';
import { SchoolAdmin } from '../../models/schooladmin';
import { Injectable } from '@angular/core';


@Injectable()

export class AddUserHttpService {
    constructor(private _httpService: HttpService)    { }

    addUser(addStudentViewModel: any): Observable<any> {
        return this._httpService.postRequest(AppConfig.API_ENDPOINT_URL + 'schooladmin/adduser', JSON.stringify(addStudentViewModel));
    }
}
