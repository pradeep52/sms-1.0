export class Student
{
    public StudentId: string;

    public FirstName:string;

    public MiddleName: string;

    public LastName: string;

    public DOB: string;

    public Gender: string;

    public RollNumber: string;

    public SchoolId: string;    

    public GuardianId:string;

    public AddressId: string;

    public CasteId: string;

    public StandardId: string;

    public SectionId: string;

}