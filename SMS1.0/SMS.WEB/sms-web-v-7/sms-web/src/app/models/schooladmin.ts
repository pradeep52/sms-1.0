export class SchoolAdmin {
    
    public SchoolAdminId: string;

    public SchoolId: string;

    public FirstName: string;

    public MiddleName: string;

    public LastName: string;

    public Email: string;

    public UserName: string;

    public Password: string;

    public ConfirmPassword: string;

    public PrimaryContactNo: string;

    public SecondaryContactNo: string;

    public IsSchoolSuperAdmin:boolean;

    public IsForgotPassword:boolean;

    public CreatedOn: any;

    public RemovedOn: any;

    public CreatedBy: string;

    public IsActive: boolean;
}