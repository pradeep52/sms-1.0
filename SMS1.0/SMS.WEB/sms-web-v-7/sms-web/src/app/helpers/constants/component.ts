export class ComponentConstants
{
    static readonly LoginComponent = "login";
    static readonly DashboardComponent = "dashboard";
    static readonly RegisterComponent = "register";
}