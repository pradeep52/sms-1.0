import { SmsWebPage } from './app.po';

describe('sms-web App', () => {
  let page: SmsWebPage;

  beforeEach(() => {
    page = new SmsWebPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
