import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './app-routing/app-routing.module';
import 'hammerjs';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpModule} from '@angular/http';
import {HttpService} from './helpers/service/http.service';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AdduserComponent } from './components/adduser/adduser.component';
import { LeftnavComponent } from './components/leftnav/leftnav.component';
import { RegisterComponent } from './components/register/register.component';
import { HeaderComponent } from './components/header/header.component';
import { DashboardstatsComponent } from './components/dashboardstats/dashboardstats.component';
import { AddstandardComponent } from './components/addstandard/addstandard.component';
import { UserlistComponent } from './components/userlist/userlist.component';
import {ManagestandardComponent} from './components/managestandard/managestandard.component';
import { AddsectionComponent } from './components/addsection/addsection.component';
import { ManagesectionComponent } from './components/managesection/managesection.component';
import { ForgotpasswordComponent } from './components/forgotpassword/forgotpassword.component';
import { ResetpasswordComponent } from './components/resetpassword/resetpassword.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    AdduserComponent,
    LeftnavComponent,
    RegisterComponent,
    HeaderComponent,
    DashboardstatsComponent,
    AddstandardComponent,
    UserlistComponent,
    ManagestandardComponent,
    AddsectionComponent,
    ManagesectionComponent,
    ForgotpasswordComponent,
    ResetpasswordComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [HttpService],
  bootstrap: [AppComponent]
})
export class AppModule { }
