import { Injectable } from '@angular/core';
import { Http, Response, Headers,ConnectionBackend, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';


@Injectable()

export class HttpService
{
    constructor(private _http:Http) {
        //super(backend, defaultOptions);
  }

    getRequest(url: any): Observable<any> {
        var token = localStorage.getItem('JWT');
        var userHeader: Headers = new Headers();
        userHeader.append('Authorization', "Bearer " + token);
        return this._http.get(url, { headers: userHeader })
            .map(this.extractData)
            .catch((error) => {
                return this.handleError(error);
            });
    }

    postRequest(url: string, filter : any): Observable<any> {
        var token = localStorage.getItem('JWT');
        var userHeader: Headers = new Headers({'Content-Type': 'application/json'});
        userHeader.append('Authorization', "Bearer " + token);
        return this._http.post(url, filter, { headers: userHeader })
            .map(this.extractData)
            .catch((error) => {
                return this.handleError(error);
            });
    }

    private extractData(res: Response) {
        if (res.status < 200 || res.status >= 300) {
            throw new Error('Bad response status: ' + res.status);
        }
        let body = res.json();
        return body || {};
    }

    private handleError(error: any) {
        // In a real world app, we might send the error to remote logging infrastructure
        if (error.status == 401) {
            localStorage.removeItem('JWT');
            //window.location.reload();
            console.log(error);
        } else {
            var errorInfo = JSON.parse(error._body);
            if (errorInfo.Message) {
                var _that = this;
                return Observable.throw(errorInfo.Message);        
            }
        }
        
        let errMsg = error.Message || 'Critical error occured on the server, during the processing of your request';
        console.error(errMsg); // log to console instead
        var _that = this;
        return Observable.throw(errMsg);
    }
}