import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from '../app.component';
import { LoginComponent } from '../components/login/login.component';
import { DashboardComponent } from '../components/dashboard/dashboard.component';
import { AdduserComponent } from '../components/adduser/adduser.component';
import { RegisterComponent } from '../components/register/register.component';
import { DashboardstatsComponent } from '../components/dashboardstats/dashboardstats.component';
import { AddstandardComponent } from '../components/addstandard/addstandard.component';
import { UserlistComponent } from '../components/userlist/userlist.component';
import { ManagestandardComponent } from '../components/managestandard/managestandard.component';
import { AddsectionComponent } from '../components/addsection/addsection.component';
import { ManagesectionComponent } from '../components/managesection/managesection.component';
import { ForgotpasswordComponent } from '../components/forgotpassword/forgotpassword.component';
import { ResetpasswordComponent } from '../components/resetpassword/resetpassword.component';


const routes: Routes = [
    {
        path: '',
        component: LoginComponent,
    },
    {
        path: 'dashboard',
        component: DashboardComponent,
        children: [
            {
                path: '',
                component: DashboardstatsComponent
            },
            {
                path: 'add-user',
                component: AdduserComponent
            },
            {
                path: 'user-list',
                component: UserlistComponent
            },
            {
                path: 'add-standard',
                component: AddstandardComponent
            },
            {
                path: 'manage-standard',
                component: ManagestandardComponent
            },
            {
                path: 'add-section',
                component: AddsectionComponent
            },
            {
                path: 'manage-section',
                component: ManagesectionComponent
            }
        ]
    },
    {
        path: 'register',
        component: RegisterComponent,
    },
    {
        path: 'forgotpassword',
        component: ForgotpasswordComponent,
    },
    {
        path: 'resetpassword',
        component: ResetpasswordComponent,
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {useHash : true})
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})
export class AppRoutingModule { }