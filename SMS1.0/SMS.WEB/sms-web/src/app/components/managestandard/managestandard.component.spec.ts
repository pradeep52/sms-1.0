import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagestandardComponent } from './managestandard.component';

describe('ManagestandardComponent', () => {
  let component: ManagestandardComponent;
  let fixture: ComponentFixture<ManagestandardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagestandardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagestandardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
