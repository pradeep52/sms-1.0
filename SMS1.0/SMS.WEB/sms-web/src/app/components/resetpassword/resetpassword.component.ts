import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ResetPasswordService } from './resetpassword.service';
import { ValidationSummary } from '../../models/validationsummary';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css'],
  providers: [ResetPasswordService]
})
export class ResetpasswordComponent implements OnInit {
public OldPassword: string;
public NewPassword: string;
public ChangedPassword: any;
public MatchedPassword: any;
public ConfirmPassword: string;
public ValidationMessage: string;
public Message: string;
public ValidationSummary: ValidationSummary;

  constructor(private _router: Router, private _resetPasswordService: ResetPasswordService) { }

  ngOnInit() {
  }

  public ChangePassword()
  {
    this.ChangedPassword = {
      OldPassword:  this.OldPassword,
      NewPassword:  this.NewPassword
    };
    this.MatchedPassword= {
      NewPassword: this.NewPassword,
      ConfirmPassword: this.ConfirmPassword,
      OldPassword: this.OldPassword
    };
    var result;
    this.ValidationSummary = this._resetPasswordService.validatefields(this.MatchedPassword);
    if(this.ValidationSummary.IsValid){
    this._resetPasswordService.changepassword(this.ChangedPassword).subscribe(changedResult => result = changedResult, error => console.log(error), () => this.OnPasswordChange(result));
    }
  }

  private OnPasswordChange(res: any)
  {
    if(res && res.IsSuccess)
      {
        this.Message = res.Result;
      }
    else {
      this.ValidationMessage = "Password not changed";
       }
  }
}
