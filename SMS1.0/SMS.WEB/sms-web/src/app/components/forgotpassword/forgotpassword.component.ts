import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ForgotPasswordService } from './forgotpassword.service';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css'],
  providers: [ForgotPasswordService]
})
export class ForgotpasswordComponent implements OnInit {
  public email: string;
  public ValidationMessage: string;
  public Message: string;

  constructor(private _router: Router, private _forgotpasswordService:ForgotPasswordService) { }

  ngOnInit() {

  }
  public Submit()
  {
    var res;
    this._forgotpasswordService.resetpassword(this.email)
    .subscribe(passwordResult => res = passwordResult, error => this.OnError(), () => this.OnResetPassword(res));
  }

  private OnError() {
    this.ValidationMessage = "Invalid EmailId";
  }

  private OnResetPassword(res: any)
  {
    if(res && res.IsSuccess)
      {
        this.Message = res.Result;
      }
  else {
      this.ValidationMessage = "Invalid EmailId";
       }
  }
}
