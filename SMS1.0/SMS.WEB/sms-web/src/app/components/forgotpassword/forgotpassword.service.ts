import { Injectable,Component } from '@angular/core';
import { Http, Response,Headers,URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../../helpers/service/http.service';
import { AppConfig } from '../../helpers/constants/app.config';


@Injectable()

export class ForgotPasswordService
{
    constructor(private _http:Http,private _httpService:HttpService){ }

    resetpassword(email:string):Observable<any>{
        return this._httpService.postRequest(AppConfig.API_ENDPOINT_URL + 'login/resetpassword',JSON.stringify(email));
    }
}