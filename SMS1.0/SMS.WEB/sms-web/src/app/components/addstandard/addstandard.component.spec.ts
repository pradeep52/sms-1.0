import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddstandardComponent } from './addstandard.component';

describe('AddstandardComponent', () => {
  let component: AddstandardComponent;
  let fixture: ComponentFixture<AddstandardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddstandardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddstandardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
