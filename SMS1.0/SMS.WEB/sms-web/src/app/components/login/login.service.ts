import { Injectable,Component } from '@angular/core';
import { Http, Response,Headers,URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../../helpers/service/http.service';
import { AppConfig } from '../../helpers/constants/app.config';


@Injectable()

export class LoginService
{
    constructor(private _http:Http,private _httpService:HttpService){ }

    login(credentials:any):Observable<any>{
        return this._httpService.postRequest(AppConfig.API_ENDPOINT_URL + 'login/login',JSON.stringify(credentials));
    }

    authenticate(credentials:any):Observable<any>{
        var headers = new Headers({'Content-Type':'application/json'});
        var data = "username=" + credentials.UserName + "&password=" + credentials.Password + "&grant_type=password";
        return this._http.post(AppConfig.API_TOKEN_URL + '/token',data,{headers:headers});
    }
}