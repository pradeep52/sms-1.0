import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ComponentConstants } from '../../helpers/constants/component';
import { GeneralFunctions } from '../../helpers/generalfunctions';
import * as jQuery from 'jquery';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {
  public UserName: string;
  public Password: string;
  public credentials: any;
  public LoginResult: any;
  public ValidationMessage: string;

  constructor(private _router: Router, private _loginService:LoginService) { }

  ngOnInit() {
    this.ApplyLoginTheme();
  }
  public Login() {
    this.credentials = {
        UserName: this.UserName,
        Password: this.Password
    };
    var res;
    this._loginService.login(this.credentials)
    .subscribe(loginResult => res = loginResult, error => this.OnLoginError(), () => this.OnLogin(res));
}
private OnLogin(res: any) {
  if(res && res.IsSuccess)
   {
      if(res.Result.IsForgotPassword){
        this._router.navigate(['/resetpassword']);
      }
      else {
        localStorage.setItem('UserInfo', JSON.stringify(res.Result));
        var authResult = {};
        this._loginService.authenticate(this.credentials)
          .subscribe(_authResult => authResult = _authResult, error => console.log(error), () => this.OnTokenGenerated(authResult));
           }
  }
      else {
        this.ValidationMessage = "Invalid Username/Password";
          }

}

private OnTokenGenerated(authResult: any) {
  var _result = authResult.json();
  if(authResult){
      localStorage.setItem('JWT', _result.access_token);
      localStorage.setItem('JWTExpiry', _result.expires_in);
      this._router.navigate(['/dashboard']);
  }
  
}  

private OnLoginError() {
  this.ValidationMessage = "Invalid Username/Password";
}

private ApplyLoginTheme() {
  let elem = jQuery("body");
  GeneralFunctions.ChangeBodyClass(elem, ComponentConstants.LoginComponent);
}
}
