import { Component, OnInit } from '@angular/core';
import { UserListHttpService } from '../../components/userlist/userlist.service'
import { SchoolAdmin } from '../../models/schooladmin'

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css'],
  providers: [UserListHttpService]
})
export class UserlistComponent implements OnInit {
  public Users: Array<SchoolAdmin>[]= [];
  constructor(private userListService: UserListHttpService) { }

  ngOnInit() {
    this.GetUsers();
  }

  private GetUsers()
  {
    var _userInfo = localStorage.getItem('UserInfo');
    let _loggedUser = JSON.parse(_userInfo);
    var res;
    this.userListService.getUsers(_loggedUser.SchoolId)
    .subscribe(result => res = result, error => console.log(error), () => this.OnGetUsersComplete(res));
  }

  private OnGetUsersComplete(userListResult: any)
  {
    this.Users = userListResult.Result.Result;
  }

}
