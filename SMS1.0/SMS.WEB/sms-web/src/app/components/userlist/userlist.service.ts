import { Injectable,Component } from '@angular/core';
import { Http, Response,Headers,URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../../helpers/service/http.service';
import { AppConfig } from '../../helpers/constants/app.config';
import { School } from '../../models/school';

@Injectable()

export class UserListHttpService
{
    constructor(private _http: Http, private _httpService: HttpService) { }

    getUsers(schoolId: string): Observable<any> {
        return this._httpService.getRequest(AppConfig.API_ENDPOINT_URL + 'schooladmin/getusers?schoolId=' + schoolId);
    }
}
