import { SchoolAdmin } from '../../models/schooladmin';
import { ValidationSummary } from '../../models/validationsummary';

export class AddUserService {
    public static ValidateFields(schoolAdmin: SchoolAdmin): ValidationSummary {
        let validationSummary: ValidationSummary = new ValidationSummary();
        if (!schoolAdmin) {
            validationSummary.IsValid = false;
            validationSummary.Messages.push('Please enter all mandatory fields');
        } else {
            if (!schoolAdmin.FirstName || !schoolAdmin.LastName || !schoolAdmin.UserName
                || !schoolAdmin.Email || !schoolAdmin.Password || !schoolAdmin.PrimaryContactNo) {
                validationSummary.IsValid = false;
                validationSummary.Messages.push('Please fill in (*) mandatory fields');
            }

            if (schoolAdmin.Password !== schoolAdmin.ConfirmPassword) {
                validationSummary.IsValid = false;
                validationSummary.Messages.push('Passwords do not match');
            }
        }
        return validationSummary;
    }
}
