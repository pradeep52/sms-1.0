import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AddUserService } from './adduserservice';
import { SchoolAdmin } from '../../models/schooladmin';
import { ValidationSummary } from '../../models/validationsummary';
import { AddUserHttpService } from './adduser.http.service';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css'],
  providers: [AddUserHttpService]
})
export class AdduserComponent implements OnInit {
  public SchoolAdmin: SchoolAdmin;
  public AddUserResult: any;
  public ValidationSummary: ValidationSummary;

  constructor(private _router: Router, private _addUserHttpService: AddUserHttpService) {
   }

  ngOnInit() {
    this.PrePopulateUser();
  }

  public CreateUser() {
    this.ValidationSummary = AddUserService.ValidateFields(this.SchoolAdmin);
    if (this.ValidationSummary.IsValid) {
      this._addUserHttpService.addUser(this.SchoolAdmin)
      .subscribe(result => this.AddUserResult = result, error => console.log(error), () => this.OnAddUserComplete());
    }
  }

  private PrePopulateUser() {
    let _userInfo = localStorage.getItem('UserInfo');
    let _loggedUser = JSON.parse(_userInfo);
     this.SchoolAdmin = new SchoolAdmin();
     this.SchoolAdmin.SchoolId = _loggedUser.SchoolId;
     this.SchoolAdmin.IsSchoolSuperAdmin = false;
  }

  OnAddUserComplete(): any {
    this._router.navigate(['/dashboard/user-list']);
  }

}
