import { Guardian } from './guardian';
import { Student } from './student';
import { Religion } from './religion';
import { Caste } from './caste';

export class StudentRegistration
{
    public Guardian:Guardian;
    public Student:Student;
    public Religion: Religion;
    public Caste: Caste;
}