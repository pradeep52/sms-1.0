export class School
{
    public SchoolId:string;

    public Name:string;

    public Address:string;

    public Country:string;

    public State:string;

    public City:string;

    public Email:string;

    public PrimaryContactNo:string;

    public SecondaryContactNo:string;
    
}