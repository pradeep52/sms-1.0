export class Guardian
{
    public GuardianId: string;

    public SchoolId: string;

    public FirstName: string;

    public MiddleName: string;

    public LastName: string;

    public Gender: string;

    public RelationWithStudent: string;

    public EmailId: string;

    public PrimaryContactNumber: string;

    public SecondaryContactNumber: string;

    public Occupation: string;
}