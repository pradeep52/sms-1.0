﻿using SMS.BAL.Helpers;
using SMS.DATA;
using System;
using System.Collections.Generic;

namespace SMS.INITIALTABLEINSERT
{
    class Program
    {
        public School School;
        public SchoolAdmin SchoolAdmin;
        public List<Religion> Religions = new List<Religion>();
        public List<Caste> Castes = new List<Caste>();

        static void Main()
        {
            Program p = new Program();
            p.InitializeSeed();
        }

        public void InitializeSeed()
        {
            smsDBContext _dbContext = new smsDBContext();

            #region School
            this.School = new School()
            {
                Address = "Bangalore, WhiteField",
                City = "Bangalore",
                Country = "India",
                Email = "info@kv.com",
                Name = "Kendriya Vidyala",
                State = "Karnataka",
                PrimaryContactNo = "9897997955",
                SchoolId = Guid.NewGuid()
            };
            #endregion

            #region SchoolAdmin
            this.SchoolAdmin = new SchoolAdmin()
            {
                Email = "admin@kv.com",
                LastName = "Walker",
                MiddleName = "P",
                FirstName = "John",
                Password = Helper.PasswordEncryption("1234"),
                UserName = "admin",
                PrimaryContactNo = "9999999999",
                IsSchoolSuperAdmin = true,
                SchoolAdminId = Guid.NewGuid(),
                SchoolId = this.School.SchoolId,
                IsForgotPassword = false,
                SecondaryContactNo = "39849039499",
                CreatedOn = DateTime.Now,
                CreatedBy = Guid.NewGuid(),
                IsActive = true
            };
            #endregion

            #region Religion
            this.Religions.Add(this.GetReligion("Hindu"));
            this.Religions.Add(this.GetReligion("Muslim"));
            this.Religions.Add(this.GetReligion("Christian"));
            #endregion

            #region Caste
            for (int i = 0; i < this.Religions.Count; i++)
            {
                if (i == 0)
                {
                    this.Castes.Add(this.GetCaste(this.Religions.ToArray()[i].ReligionId, "Brahmin", "Smarth", "GM"));
                    this.Castes.Add(this.GetCaste(this.Religions.ToArray()[i].ReligionId, "Lingayat", "Panchamasali", "GM"));
                    this.Castes.Add(this.GetCaste(this.Religions.ToArray()[i].ReligionId, "Gowdas", "Vokkaliga", "2A"));
                }
                else if (i == 1)
                {
                    this.Castes.Add(this.GetCaste(this.Religions.ToArray()[i].ReligionId, "Siddiqui", "Navayat", "OBC"));
                    this.Castes.Add(this.GetCaste(this.Religions.ToArray()[i].ReligionId, "Baghban", "Bohra ", "OBC"));
                    this.Castes.Add(this.GetCaste(this.Religions.ToArray()[i].ReligionId, "Dard", "Mughal", "OBC"));
                }
                else if (i == 2)
                {
                    this.Castes.Add(this.GetCaste(this.Religions.ToArray()[i].ReligionId, "Roman", "Smarth", "GM"));
                    this.Castes.Add(this.GetCaste(this.Religions.ToArray()[i].ReligionId, "Catholics", "Panchamasali", "GM"));
                    this.Castes.Add(this.GetCaste(this.Religions.ToArray()[i].ReligionId, "Othodox ", "Vokkaliga", "GM"));
                }
                
            }
            #endregion




            using (var transaction = _dbContext.Database.BeginTransaction())
            {
                _dbContext.School.Add(this.School);
                _dbContext.SchoolAdmin.Add(this.SchoolAdmin);
                _dbContext.Religion.AddRange(this.Religions);
                _dbContext.Caste.AddRange(this.Castes);
                _dbContext.SaveChanges();
                transaction.Commit();

                Console.WriteLine("Seed data added succesfully");
                Console.ReadKey();
            }
        }

        private Religion GetReligion(string religionName)
        {
            return new Religion()
            {
                ReligionId = Guid.NewGuid(),
                ReligionName = religionName
            };
        }

        private Caste GetCaste(Guid religionId, string casteName, string subCasteName, string category)
        {
            return new Caste()
            {
                CasteId = Guid.NewGuid(),
                CasteName = casteName,
                Category = category,
                ReligionId = religionId,
                SubCasteName = subCasteName
            };
        }
    }
}
