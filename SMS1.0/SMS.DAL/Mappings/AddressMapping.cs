﻿namespace SMS.DAL.Mappings
{
    using SMS.DATA;
    using System.Data.Entity.ModelConfiguration;
    public class AddressMapping : EntityTypeConfiguration<Address>
    {
        public AddressMapping()
        {
            ToTable("Address");
            HasKey(address => address.AddressId);
            Property(address => address.Country).IsRequired();
            Property(address => address.State).IsRequired();
            Property(address => address.City).IsRequired();
            Property(address => address.Description).IsRequired();
        }
    }
}
