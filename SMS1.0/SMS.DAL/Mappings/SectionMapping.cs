﻿namespace SMS.DAL.Mappings
{
    using SMS.DATA;
    using System.Data.Entity.ModelConfiguration;
    class SectionMapping : EntityTypeConfiguration<Section>
    {
        public SectionMapping()
        {
            ToTable("Section");
            HasKey(section => section.SectionId);
            Property(section => section.SectionId)
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            HasRequired(section => section.School)
                .WithMany(school => school.Sections)
                .HasForeignKey(schoolAdmin => schoolAdmin.SchoolId);
            Property(section => section.Name).IsRequired();
        }
    }
}
