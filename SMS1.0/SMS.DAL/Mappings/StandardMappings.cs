﻿namespace SMS.DAL.Mappings
{
    using SMS.DATA;
    using System.Data.Entity.ModelConfiguration;
    public class StandardMappings : EntityTypeConfiguration<Standards>
    {
        public StandardMappings()
        {
            ToTable("Standards");
            HasKey(standard => standard.StandardId);
            Property(standard => standard.StandardId)
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(standard => standard.Name).IsRequired();
        }
    }
}
