﻿
namespace SMS.DAL.Mappings
{
    using SMS.DATA;
    using System.Data.Entity.ModelConfiguration;
    public class GuardianMapping: EntityTypeConfiguration<Guardian>
    {
        public GuardianMapping()
        {
            ToTable("Guardian");
            HasKey(guardian => guardian.GuardianId);
            Property(guardian => guardian.GuardianId).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            HasRequired(guardian => guardian.School)
                .WithMany(school => school.Guardians)
                .HasForeignKey(guardian => guardian.SchoolId);
            Property(guardian => guardian.FirstName).IsRequired();
            Property(guardian => guardian.LastName).IsRequired();
            Property(guardian => guardian.Gender).IsRequired();
            Property(guardian => guardian.RelationWithStudent).IsRequired();
            Property(guardian => guardian.Email).IsRequired();
            Property(guardian => guardian.PrimaryContactNumber).IsRequired();
            Property(guardian => guardian.Occupation).IsRequired();
        }
    }
}
