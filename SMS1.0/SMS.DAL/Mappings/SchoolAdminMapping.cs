﻿namespace SMS.DAL.Mappings
{
    using SMS.DATA;
    using System.Data.Entity.ModelConfiguration;

    public class SchoolAdminMapping : EntityTypeConfiguration<SchoolAdmin>
    {
        public SchoolAdminMapping()
        {
            this.ToTable("SchoolAdmin");
            HasKey(schoolAdmin => schoolAdmin.SchoolAdminId);
            Property(schoolAdmin => schoolAdmin.SchoolAdminId).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            HasRequired(schoolAdmin => schoolAdmin.School)
                .WithMany(school => school.SchoolAdmins)
                .HasForeignKey(schoolAdmin => schoolAdmin.SchoolId);
            Property(sa => sa.FirstName).IsRequired();
            Property(sa => sa.LastName).IsRequired();
            //Property(sa => sa.MiddleName).IsRequired();
            Property(sa => sa.Email).IsRequired();
            Property(sa => sa.UserName).IsRequired();
            Property(sa => sa.Password).IsRequired();
            Property(sa => sa.PrimaryContactNo).IsRequired();
            Property(sa => sa.IsForgotPassword).IsRequired();
            Property(sa => sa.IsSchoolSuperAdmin).IsRequired();
            Property(sa => sa.CreatedBy).IsRequired();
            Property(sa => sa.CreatedOn).IsRequired();
            Property(sa => sa.IsActive).IsRequired();
        }
    }
}
