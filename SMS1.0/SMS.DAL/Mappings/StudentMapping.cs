﻿namespace SMS.DAL.Mappings
{
    using DATA;
    using System.Data.Entity.ModelConfiguration;
    public class StudentMapping : EntityTypeConfiguration<Student>
    {
        public StudentMapping()
        {
            ToTable("Student");
            HasKey(student => student.StudentId);
            Property(student => student.FirstName).IsRequired();
            Property(student => student.LastName).IsRequired();
            Property(student => student.DOB).IsRequired();
            Property(student => student.Gender).IsRequired();
            
            // Map with Address
            HasRequired(student => student.Address)
                .WithMany(address => address.Students)
                .HasForeignKey(student => student.AddressId);
            // Map with Section
            HasRequired(student => student.Section)
                .WithMany(section => section.Students)
                .HasForeignKey(student => student.SectionId);
            // Map with Address
            HasRequired(student => student.Address)
                .WithMany(address => address.Students)
                .HasForeignKey(student => student.AddressId);
            // Map with Guardian
            HasRequired(student => student.Guardian)
                .WithMany(guardian => guardian.Students)
                .HasForeignKey(student => student.GuardianId);
            // <ap with Standard
            HasRequired(student => student.Standard)
                .WithMany(standard => standard.Students)
                .HasForeignKey(student => student.StandardId);
        }
    }
}
