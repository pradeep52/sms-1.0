﻿namespace SMS.DAL.Mappings
{
    using SMS.DATA;
    using System.Data.Entity.ModelConfiguration;
    public class CasteMapping : EntityTypeConfiguration<Caste>
    {
        public CasteMapping()
        {
            ToTable("Caste");
            HasKey(caste => caste.CasteId);
            Property(caste => caste.CasteName).IsRequired();
            Property(caste => caste.Category).IsRequired();
            HasRequired(religion  => religion.Religion)
                .WithMany(religion =>religion.Caste)
                .HasForeignKey(caste => caste.ReligionId);
        }
    }
}
