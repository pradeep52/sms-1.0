﻿using SMS.DATA;
using System.Data.Entity.ModelConfiguration;

namespace SMS.DAL.Mappings
{
    public class SchoolMapping: EntityTypeConfiguration<School>
    {
        public SchoolMapping()
        {
            this.ToTable("School");
            HasKey(school => school.SchoolId);
            Property(school => school.Name).IsRequired();
            Property(school => school.Address).IsRequired();
            Property(school => school.Country).IsRequired();
            Property(school => school.State).IsRequired();
            Property(school => school.City).IsRequired();
            Property(school => school.Email).IsRequired();
            Property(school => school.PrimaryContactNo).IsRequired();
        }
    }
}
