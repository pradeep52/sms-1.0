﻿namespace SMS.DAL.Mappings
{
    using SMS.DATA;
    using System.Data.Entity.ModelConfiguration;
    public class ReligionMapping : EntityTypeConfiguration<Religion>
    {
        public ReligionMapping()
        {
            ToTable("Religion");
            HasKey(religion => religion.ReligionId);
            Property(religion => religion.ReligionName).IsRequired();
        }
    }
}
