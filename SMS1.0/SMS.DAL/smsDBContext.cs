namespace SMS.DATA
{
    using System.Data.Entity;
    using DAL.Mappings;
    using SMS.DAL;

    public class smsDBContext : DbContext
    {
        public smsDBContext()
            : base("name=smsDBContext")
        {
            //Database.SetInitializer<smsDBContext>(new smsDBInitializer());
        }

        public DbSet<School> School { get; set; }
        public DbSet<SchoolAdmin> SchoolAdmin { get; set; }
        public DbSet<Address> Address { get; set; }
        public DbSet<Caste> Caste { get; set; }
        public DbSet<Guardian> Guardian { get; set; }
        public DbSet<Section> Section { get; set; }
        public DbSet<Standards> Standard { get; set; }
        public DbSet<Student> Student { get; set; }
        public DbSet<Religion> Religion { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new SchoolMapping());
            modelBuilder.Configurations.Add(new SchoolAdminMapping());
            modelBuilder.Configurations.Add(new AddressMapping());
            modelBuilder.Configurations.Add(new CasteMapping());
            modelBuilder.Configurations.Add(new GuardianMapping());
            modelBuilder.Configurations.Add(new SectionMapping());
            modelBuilder.Configurations.Add(new StandardMappings());
            modelBuilder.Configurations.Add(new StudentMapping());
            modelBuilder.Configurations.Add(new ReligionMapping());
        }
    }
}
