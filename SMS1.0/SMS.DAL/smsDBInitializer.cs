﻿using SMS.DATA;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMS.DAL
{
    public class smsDBInitializer : DropCreateDatabaseAlways<smsDBContext>
    {

        protected override void Seed(smsDBContext context)
        {

            IList<School> school = new List<School>();

            school.Add(new School()
            {
                Address = "Bangalore, WhiteField",
                City = "Bangalore",
                Country = "India",
                Email = "info@kv.com",
                Name = "Kendriya Vidyala",
                State = "Karnataka",
                PrimaryContactNo = "9897997955",
                SchoolId = Guid.NewGuid()
            });

            foreach (School sc in school)
                context.School.Add(sc);
            base.Seed(context);
        }
    }
}
